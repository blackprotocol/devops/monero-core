# Muitistage docker build
# builder stage
FROM ubuntu:22.04 as builder
RUN apt-get update &&  \
    apt-get upgrade -y && \
    DEBIAN_FRONTEND=noninteractive apt-get --no-install-recommends --yes install \
        automake \
        autotools-dev \
        bsdmainutils \
        build-essential \
        ca-certificates \
        ccache \
        cmake \
        curl \
        git \
        libtool \
        pkg-config \
        gperf

WORKDIR /opt/monero
RUN git clone https://gitlab.com/thorchain/tss/monero-sign.git  --depth=1 .

ARG NPROC
RUN set -ex && \
    git submodule init && git submodule update && \
    rm -rf build && \
    if [ -z "$NPROC" ] ; \
    then make -j$(nproc) depends target=x86_64-linux-gnu ; \
    else make -j$NPROC depends target=x86_64-linux-gnu ; \
    fi

FROM ubuntu:22.04

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get --yes install \
    wget \
    bzip2 \
    curl

COPY --from=builder /usr/lib/x86_64-linux-gnu/ /usr/lib/x86_64-linux-gnu/
COPY --from=builder /opt/monero/build/x86_64-linux-gnu/release/bin/ /usr/local/bin/

WORKDIR /data

RUN wget https://gui.xmr.pm/files/block.txt -q

EXPOSE 18080 18081 18082 18083
