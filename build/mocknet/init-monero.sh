#!/bin/bash

set -ex

MASTER_ADDR="${XHV_MASTER_ADDR:=45v8VYPut8S9wY9fTvn1gKiGN5AMMzsCK9kLNcEpEESygy4vU3Jf3uE4Zyqv2bDLwTToH8ArrsZ3W28kmKUKJkjY8fN9anA}"
BLOCK_TIME=${BLOCK_TIME:=5}

# Start the daemon
monerod --regtest --offline --fixed-difficulty 1 --non-interactive --rpc-bind-ip 0.0.0.0 --confirm-external-bind &

sleep 5

i=0
while [ $i -ne 50 ]; do
  curl http://127.0.0.1:18081/json_rpc -d \
    '{"jsonrpc":"2.0","id":"0","method":"generateblocks","params":{
       "amount_of_blocks": 2,
       "wallet_address": "'"${MASTER_ADDR}"'",
       "starting_nonce": 0}' \
    -H 'Content-Type: application/json'
  i=$((i + 1))
done

# mine a new block every BLOCK_TIME
while true; do
  curl http://127.0.0.1:18081/json_rpc -d \
    '{"jsonrpc":"2.0","id":"0","method":"generateblocks","params":{
	"amount_of_blocks": 1,
	"wallet_address": "'"${MASTER_ADDR}"'",
	"starting_nonce": 0}' \
    -H 'Content-Type: application/json'
  sleep $BLOCK_TIME
done
